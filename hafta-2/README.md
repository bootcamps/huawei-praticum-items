# Huawei Praticum hafta 2

Bu hafta verilen pdf kitabın ilk 366-504 sayfaları arasını çalışmamız ve koolabs üzerinden 3 adet lab`ı %100 şekilde yapmamız istendi.

## Contents
 - HCIA-Cloud+Service+V3.0+Training+Material.pdf
   - HCIA-Cloud Service V3.0 Training Material.pdf (Chapter 06 & Chapter 07)
 - https://lab.huaweicloud.com/intl/en-us/testList
   - O&M Services Practice
   - Networking Services Practice
   - Deploying an Enterprise Web Services Practice
