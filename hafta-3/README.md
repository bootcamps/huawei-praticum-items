# Huawei Praticum hafta 3

Bu hafta çalışacağımız pdf`ler değişti ve kendi hesabımızdan ödev yapmaya başlıyoruz. 

## Contents
 - HCIE-Cloud+Service+Solutions+Architect+V2.0+Training+Material.pdf(1-223)
   - Cloud-based Architecture Design (Module-1)
   - Underlying Technologies of Huawei Cloud (Module-2)
   - Overview of Application Cloud Migration (Module-3_01)
   - Cloud-based Scalability Design (Module-3_02)
 - HCIE-Cloud+Service+Solutions+Architect+V2.0+Lab+Guide.pdf()
   - Cloud-based Scalability Design (10-41) 
