# Huawei Praticum hafta 1 

Bu hafta verilen pdf kitabın ilk 366 sayfasını çalışmamız ve koolabs üzerinden 2 adet lab`ı %100 şekilde yapmamız istendi.

## Contents
 - HCIA-Cloud+Service+V3.0+Training+Material.pdf
   - HCIA-Cloud Service V3.0 Training Material.pdf (Chapter 01 & Chapter 02)
   - HCIA-Cloud Service V3.0 Training Material.pdf (Chapter 03 & Chapter 04 & Chapter 05)
 - https://lab.huaweicloud.com/intl/en-us/testList
   - Compute Services Practice
   - Storage Services Practice

