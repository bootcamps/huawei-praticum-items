FROM debian:latest

RUN apt-get update && apt-get install -y curl wget

RUN mkdir hwcloud-cli 
WORKDIR /hwcloud-cli
RUN wget https://ap-southeast-3-hwcloudcli.obs.ap-southeast-3.myhuaweicloud.com/cli/latest/huaweicloud-cli-linux-amd64.tar.gz
RUN tar xf huaweicloud-cli-linux-amd64.tar.gz
RUN ln -s $(pwd)/hcloud /usr/local/bin/

RUN sudo apt-get install -y gnupg software-properties-common
RUN wget -O- https://apt.releases.hashicorp.com/gpg | \
    gpg --dearmor | \
    sudo tee /usr/share/keyrings/hashicorp-archive-keyring.gpg
RUN gpg --no-default-keyring \
    --keyring /usr/share/keyrings/hashicorp-archive-keyring.gpg \
    --fingerprint
RUN echo "deb [signed-by=/usr/share/keyrings/hashicorp-archive-keyring.gpg] \
    https://apt.releases.hashicorp.com $(lsb_release -cs) main" | \
    sudo tee /etc/apt/sources.list.d/hashicorp.list
RUN sudo apt update && sudo apt-get install terraform

ADD scripts scripts
